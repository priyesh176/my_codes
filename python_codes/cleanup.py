#!/usr/bin/env python
import os
import glob
import datetime
import sys
from shutil import copy,copyfile,copytree,ignore_patterns
from distutils.dir_util import copy_tree
from subprocess import Popen, PIPE
import shutil
source_directory="/home/coview/cleanup/Bereinigung_BitBucket/"
target_directory="dummy"
process = Popen(["date +%d%m%Y"],shell=True, stdout=PIPE, stderr=PIPE)
stdout, stderr = process.communicate()
date_today=stdout.strip('\n')
count_31953 = 1

def list_directory_contents(path):
        process = Popen(['ls', '-lrt',path], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        print stdout
        print stderr

def download_files_cleanup_bitbucket():

        process = Popen(['git', 'reset','--hard'], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        print stdout
        print stderr

        process = Popen(['git', 'pull','ssh://git@stash.gto.intranet.db.com:7999/cotax_pst/com.db.inv.cotax.support.cleanup.git'], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        print stdout
        print stderr

def check_if_target_directory_exists(path,srnum):
        global target_directory
        os.chdir(path)
        var = 1
        #print "Before while"
        while var == 1:
                if len(srnum) != 11:
                        print("Incorrect Service request number, check and enter again: ")
                        srnum = raw_input()
                else:
                        var = 2
        files=glob.glob("*"+srnum+"*")
        #print files[0]
        #print len(files)
        if len(files) == 1:
                target_directory = files[0]
                return files[0]
        elif len(files) < 1:
                print("Target directory is not present...Exiting");
                sys.exit()
        elif len(files) > 1:
                print("More than 1 directory found with the same SR Number, Please correct")
                sys.exit()
        else:
                print("Error happend, Exiting")
                sys.exit()

def remove_empty_lines(filename):
    if not os.path.isfile(filename):
        print("{} does not exist ".format(filename))
        return
    with open(filename) as filehandle:
        lines = filehandle.readlines()

    with open(filename, 'w') as filehandle:
        lines = filter(lambda x: x.strip(), lines)
        filehandle.writelines(lines)
    return

def strip_lead_trail_spaces(filename):
        if not os.path.isfile(filename):
                print("{} does not exist ".format(filename))
                return
        with open(filename) as filehandle:
            lines = filehandle.readlines()

        with open(filename, 'w') as filehandle:
            lines = [x.strip()+'\n' for x in lines]
            filehandle.writelines(lines)
        return

def convert_dos_to_unix(filename):
        if not os.path.isfile(filename):
                print("{} doesnt exist ".format(filename))
                return
        process = Popen(['dos2unix', filename], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        print stdout
        print stderr
        fileplusquote = '"'+filename+'"'
        print fileplusquote
        os.system('sed -i \'s/\xc2\xa0/ /g \' '+fileplusquote)
        #process = Popen(['sed -i \'s/\xc2\xa0/ /g \'', '"'+filename+'"'],shell=True, stdout=PIPE, stderr=PIPE)
        #stdout, stderr = process.communicate()
        #print stdout
        #print stderr

srnum = raw_input("Enter the service request number : \n")

ret_val = check_if_target_directory_exists("/var/applications/cotax/opr",srnum)

print("Contents of source directory - "+source_directory+" before deletion:\n")

list_directory_contents("/home/coview/cleanup/Bereinigung_BitBucket/");

os.chdir("/home/coview/cleanup/Bereinigung_BitBucket/");

print ("Removing files from "+source_directory+" if any");

os.system("pwd")
os.system("rm  *")

print("Contents of source directory  - "+source_directory+"  after pull:\n")

download_files_cleanup_bitbucket()

list_directory_contents("/home/coview/cleanup/Bereinigung_BitBucket/");


if ret_val != 0:
        print("Moving the files from "+source_directory+" to /var/applications/cotax/opr/"+target_directory)
        os.chdir("/var/applications/cotax/opr/"+target_directory)
        #os.system("rm -i *")
        if len([name for name in os.listdir('.') if os.path.isfile(name)]) > 0:
                print("Some files are already present in the current directory");
                print("Do you want to remove it (y/n) : ");
                check_yn = raw_input();
                if check_yn == 'y':
                        os.system("rm *")
                elif check_yn == 'n':
                        print("Skipping the files");
                else:
                        print("Invalid input...exiting")
                        sys.exit()
        #copy_tree(source_directory,"/var/applications/cotax/opr/"+target_directory);
        os.chdir(source_directory)
        for root, dirs, files in os.walk("."):
                files = [f for f in files if not f[0] == '.']
                dirs[:] = [d for d in dirs if not d[0] == '.']
                for filename in files:
                        copy(filename,"/var/applications/cotax/opr/"+target_directory)
elif check_if_target_directory_exists("/var/applications/cotax/opr",srnum) == 1:
        sys.exit()
elif check_if_target_directory_exists("/var/applications/cotax/opr",srnum) == 2:
        print "test"


os.chdir("/home/coview/pk/");
for fname in os.listdir("/var/applications/cotax/opr/"+target_directory):
        if fname.endswith('.zip'):
                process = Popen(['./unzip_ito.sh',"/var/applications/cotax/opr/"+target_directory], stdout=PIPE, stderr=PIPE)
                stdout, stderr = process.communicate()
                print stdout
                break


os.chdir("/var/applications/cotax/opr/"+target_directory)
for root, dirs, files in os.walk("."):
        files = [f for f in files if not f[0] == '.']
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for filename in files:
            print(filename)
            if "input_trashcan" in filename:
                remove_empty_lines(filename)
                strip_lead_trail_spaces(filename)
                convert_dos_to_unix(filename)
                file_orig=filename
                filename=filename.split(".")[0]
                file_for=filename[-3:].upper()
                output_file="trashcan_delete_"+file_for+".sql"
                process = Popen(['perl','/var/applications/cotax/opr/scripts/make_genscript.pl', '-in', file_orig, '--sql','1','--out',output_file], stdout=PIPE, stderr=PIPE)
                stdout, stderr = process.communicate()
                if os.path.exists(file_orig):
                        os.remove(file_orig)
            elif "pending_stornos_input" in filename:
                remove_empty_lines(filename)
                strip_lead_trail_spaces(filename)
                convert_dos_to_unix(filename)
                output_file="delete_pending_stornos.sql"
                process = Popen(['perl','/var/applications/cotax/opr/scripts/make_genscript.pl', '-in', filename, '--sql','7','--out',output_file], stdout=PIPE, stderr=PIPE)
                stdout, stderr = process.communicate()
                if os.path.exists(filename):
                        os.remove(filename)
            elif "31953" in filename:
                remove_empty_lines(filename)
                strip_lead_trail_spaces(filename)
                convert_dos_to_unix(filename)
                output_file="Aufruf_31953_TB_FSA_NV_BALANCE_ohne_FU_"+str(count_31953)+"_"+date_today+"_ZAS.sql"
                count_31953 = count_31953 + 1
                process = Popen(['mv',filename,output_file], stdout=PIPE, stderr=PIPE)
                stdout, stderr = process.communicate()
                with open(output_file) as filehandle:
                        lines = filehandle.readlines()
                        print lines
                with open(output_file, 'w') as filehandle:
                        lines = ['@/var/applications/COTAX/opr/scripts/31953_TB_FSA_NV_BALANCE_ohne_FU.sql '+x for x in lines]
                                #print lines
                        filehandle.writelines(lines)
            elif "wmi_trashcan" in filename:
                remove_empty_lines(filename)
                strip_lead_trail_spaces(filename)
                convert_dos_to_unix(filename)
                file_orig=filename
                filename=filename.split(".")[0]
                file_for=filename[-3:].upper()
                output_file="trashcan_delete_"+file_for+".sql"
                process = Popen(['perl','/var/applications/cotax/opr/scripts/make_genscript.pl', '-in', file_orig, '--sql','1','--out',output_file], stdout=PIPE, stderr=PIPE)
                stdout, stderr = process.communicate()
                if os.path.exists(file_orig):
                        os.remove(file_orig)

            else:
                convert_dos_to_unix(filename)
