#!/bin/bash
cd $1
rm cumaltive.txt
for i in `echo TCR*.zip`
do
unzip -P "trash" $i
file_name=`echo $i|cut -d'.' -f1`
file_name=${file_name}.txt
cat $file_name >> input_trashcan_ITO
rm $file_name
rm $i
done
sleep 2

coview@dbkpcotaxf11:~/pk$ cat runsql.sh
#!/bin/bash
. /home/oracle/ora_env

sqlplus -s "user"/"password"@db <<EOF

SET PAGESIZE 50000
SET COLSEP ","
SET LINESIZE 200
SET FEEDBACK OFF

SPOOL result.txt

select * from (select df.tablespace_name "Tablespace",
       totalusedspace "Used MB",
       (df.totalspace - tu.totalusedspace) "Free MB",
       df.totalspace "Total MB",
       round(100 * ( (tu.totalusedspace)/ df.totalspace)) "Pct. Used"
  from (select tablespace_name,
               round(sum(bytes) / 1048576) TotalSpace
          from dba_data_files
         group by tablespace_name) df,
       (select round(sum(bytes)/(1024*1024)) totalusedspace,
               tablespace_name
          from dba_segments
         group by tablespace_name) tu
 where df.tablespace_name = tu.tablespace_name
   and df.totalspace <> 0 order by 5 desc) where rownum < 10  ;

spool off

exit

EOF

