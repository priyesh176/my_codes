#!/bin/bash

echo -e "Subject: $1"
echo -e "From: $2"
echo -e "To: $3"
echo -e "Content-Type: text/html; charset=us-ascii\n"

echo -e "<pre>"
echo -e "Hello Colleagues,\n"
echo -e "The file bctplogs_2018-12-12_074847.zip was sent to tribdbk_extern@sftp1.ad.xglobal-fra.com:/sftp_tribdbkextern_inout/toxtb/bctp_logs\n"
echo -e "This email was system generated, please do not reply!\n"
echo -e "</pre>"
cat support_signature.html

